from helper import *
from os.path import join
import nltk

def main():
    dirIn = 'Corpus/Raw'
    dirOut = 'Corpus/Sentences'
    files = collect_files(dirIn, 'txt')

    for file in files:
        outRoot = file['dir'].replace(dirIn, dirOut, 1)
        outFilepath = join(outRoot, file['filename'])
        language = file['dir'][-2] + file['dir'][-1]

        content = read_fileByLine(file['path'])
        sentences = sent_tokenize_document(content, language)
        
        write_txt(sentences, outFilepath)
        console_log(f'Sentence tokenization of {file["path"]} done.')
    

def sent_tokenize_document(document: list, language: str):
    if len(document) == 0 or document == None:
        raise ValueError('Document is empty')
    
    if language == 'en':
        _lang = 'english'
    elif language == 'de':
        _lang = 'german'
    
    sentences = []
    for line in document:
        sentences = sentences + sent_tokenize_custom(line, language=_lang)
    return sentences


def sent_tokenize_custom(line: str, language: str):
    extra_abbrev_en = ['ca', 'approx', 'fig', 'tab', 'ref', 'al', 'etc']
    extra_abbrev_de = ['ca', 'approx', 'abb', 'tab', 'ref', 'al', 'z', 'B', 'etc', 'usw']

    sentence_tokenizer_en = nltk.data.load('tokenizers/punkt/english.pickle')
    sentence_tokenizer_de = nltk.data.load('tokenizers/punkt/german.pickle')

    sentence_tokenizer_en._params.abbrev_types.update(extra_abbrev_en)
    sentence_tokenizer_de._params.abbrev_types.update(extra_abbrev_de)

    if language.lower() == 'english':
        return sentence_tokenizer_en.tokenize(line)
    elif language.lower() == 'german':
        return sentence_tokenizer_de.tokenize(line)
    
    return []


if __name__ == "__main__":
    main()
