from helper import *

def main():
    filesEN = collect_files('Results/Angewandte_en', '_aligned.csv')
    corpus = []
    for fileEN in filesEN:
        angewandtePaper = {'name': fileEN['filename'].replace('_aligned.csv', ''),
                            'EN': read_csvDict(fileEN['path']), 
                            'DE': read_csvDict(fileEN['path'].replace('Angewandte_en', 'Angewandte_de')), 
                            'EN->DE': read_csvDict(fileEN['path'].replace('Angewandte_en', 'Angewandte_DeepL_de')) }
        corpus.append(angewandtePaper)

    langs = ['EN', 'DE', 'EN->DE']
    classifiers = ['NBC', 'SVC']
    basic_caluclate(corpus, langs, classifiers)

    comparsion = compare(corpus, langs, classifiers)

    histogram_langugage(comparsion, langs, classifiers)
    histogram_classifier(corpus, langs, classifiers)
    return


def histogram_classifier(corpus, langs, classifiers):
    results = []
    groups = ['none', 'NBC', 'SVC', 'both']

    for lang in langs:
        result = []
        for article in corpus:
            temp_result = {'name': article['name'],
                           'num_sentences': len(article[lang])}
            for group in groups:
                temp_result[group] = 0

            for sentence in article[lang]:
                if sentence['NBC_Label'] == sentence['SVC_Label']:
                    if sentence['NBC_Label'] == 'NO-HEDGING':
                        temp_result['none'] += 1
                    else:
                        temp_result['both'] += 1
                else:
                    if sentence['NBC_Label'] == 'NO-HEDGING':
                        temp_result['SVC'] += 1
                    else:
                        temp_result['NBC'] += 1
            result.append(temp_result)
        results.append( {'lang': lang, 'result': results} )
        write_csvDict(result, f'__Auswertung__/Angewandte/Histogram/{lang.replace(">", "")}.csv')
    return results


def histogram_langugage(compared_articles: dict, langs: list, classifiers: list):
    results = {}
    for classifier in classifiers:
        results[classifier] = []
        groups = []
        for article in compared_articles:
            result = {'name': article['name'],
                    'num_sentences': article['num_sentences']}
            for sentence in article[classifier]:
                group = sentence['group']
                try:
                    result[group] += 1
                except KeyError:
                    result[group] = 1
                groups.append(group)
            results[classifier].append(result)

        groups = list(dict.fromkeys(groups))
        groups.sort(key=lambda k: len(k))
        header = ['name', 'num_sentences'] + groups + [str(i) for i in range(len(langs)+1)]

        for article in results[classifier]:
            for head in header:
                if head not in article.keys():
                    article[head] = 0
            
            for group in groups:
                if group == 'No':
                    article['0'] = article['No']
                else:
                    plus = group.count('+') + 1
                    article[str(plus)] += article[group]
       
        write_csvDict(results[classifier], f'__Auswertung__/Angewandte/Histogram/{classifier}.csv', header=header)
    return results


def compare(files: list, langs: list, classifiers: list):
    results = []
    for file in files:
        results.append({'name': file['name'], 
                        'num_sentences': len(file[langs[0]])} )
        for classifier in classifiers:
            result = []
            for s in range(len(file[langs[0]])):
                result.append( {'sentence': s+1} )
                for lang in langs:
                    result[-1][lang] = file[lang][s][classifier + '_Label']
                result[-1]['group'] = compare_sentence(result[-1], langs)
            results[-1][classifier] = result
            write_csvDict(result, f'__Auswertung__/Angewandte/Histogram/{classifier}/{file["name"]}.csv')

    return results


def compare_sentence(sentence: dict, langs: list):
    category = ''
    for lang in langs:
        if sentence[lang] == 'HEDGING':
            category += lang + ' + '
    if len(category) > 3:
        return category[:-3]
    else:
        return 'No'


def basic_caluclate(files: list, langs: list, classifiers: list):
    results = []
    for file in files:
        results.append( {'name': file['name'],
                       'num_sentences': len(file[langs[0]])} )
        
        for classifier in classifiers:
            for lang in langs:
                num_Hedging = 0
                for sentence in file[lang]:
                    if sentence[classifier + '_Label'] == 'HEDGING':
                        num_Hedging += 1
                results[-1][lang + '_' + classifier +'_num'] = num_Hedging
                results[-1][lang + '_' + classifier +'_per'] = num_Hedging / len(file[lang])
            
    write_csvDict(results, '__Auswertung__/Angewandte/angewandte.csv')
    return results


if __name__ == '__main__':
    main()
