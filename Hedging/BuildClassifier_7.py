from helper import *
from nltk.classify import NaiveBayesClassifier, SklearnClassifier
from sklearn.svm import SVC


def main():
    bioscopeEN = [(sentence['features'], sentence['category']) for sentence in read_pickledFile('BioScope/bioscope_en.fset')]
    bioscopeDE = [(sentence['features'], sentence['category']) for sentence in read_pickledFile('BioScope/bioscope_de.fset')]

    create_dir('Classifier')
    langs = {'en': (bioscopeEN, pow(10, -0.5)), 'de': (bioscopeDE, 1)}

    for (lang, (bioscope, c)) in langs.items():
        nbc = NaiveBayesClassifier.train(bioscope)
        save_classifier(nbc, f'Classifier/nbc-{lang}.classifier')
        console_log(f'Training of Naive Bayes Classifier (lang={lang}) done.')
                  
        svc = SklearnClassifier(SVC(C=c, kernel='linear', class_weight='balanced', cache_size=2000)).train(bioscope)
        save_classifier(svc, f'Classifier/svc-{lang}.classifier')
        console_log(f'Training of Support Vector Classifier (C={c}, lang={lang}) done.')
   
    return


def save_classifier(content, filepath: str):
    with open(filepath, 'wb') as file:
        pickle.dump(content, file)
    return


if __name__ == "__main__":
    main()
