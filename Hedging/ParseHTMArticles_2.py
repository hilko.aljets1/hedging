from helper import *
from os.path import join, split
from trafilatura import extract
import trafilatura.settings

trafilatura.settings.MANUALLY_CLEANED += 'a href'
startwords = ['Einleitung', 'Introduction']
endwords = ['Abkürzungen', 'Abbreviations', 'Danksagung', 'Acknowledgments', 'Acknowledgements', 'Author Contributions', 'Supporting information', 'References']


def main():
    dirIn = 'Corpus/HTM'
    dirOut = 'Corpus/Raw'
    files = collect_files(dirIn, 'htm')

    stwords = upper_wordlist(startwords)
    edwords = upper_wordlist(endwords)

    for file in files:
        out = file['dir'].replace(dirIn, dirOut, 1)
        create_dir(out)
        parse_htm(file['path'], out, stwords, edwords)
        console_log(f'Parsed file {file["path"]}')
    return


def parse_htm(filepath: str, outDir: str, startwords: list, endwords: list):
    filename = split(filepath)[1]
    fileOut = join(outDir, filename.replace('.htm', '.txt'))

    with open(filepath, encoding='utf-8') as file:
        text = extract(file.read(), include_tables=False)
        start = len(text)
        
        # Cut beginning of the article
        for startword in startwords:
            i = text.find(startword)
            if 0 <= i < start:
                start = i
        if start == len(text):
            start = 0

        # Cut ending of the article
        end = len(text)
        for endword in endwords:
            i = text.find(endword)
            if start < i < end:
                end = i

    with open(fileOut, 'w', encoding='utf-8') as file:
        file.write(text[start:end])

    return


def upper_wordlist(words: list[str]):
    words_upper = []
    for word in words:
        words_upper.append(word)
        words_upper.append(word.upper())
    
    return words_upper


if __name__ == '__main__':
    main()
