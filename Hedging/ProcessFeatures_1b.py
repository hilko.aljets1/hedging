from helper import *
import re
from pprint import pprint
import pickle


def main():
    path = 'Features'
    
    # German Features
    featuresDE = clean_and_split(read_csvDict(path + '\\features_de.csv', delimiter=';'),['alt 1', 'alt 2', 'alt 3', 'alt 4'])
    featuresDE = build_featurelist(featuresDE)
    featuresDE.sort(key=sort_features)
    with open(path + '\german.features', 'wb') as file:
        pickle.dump(featuresDE, file)
    console_log('Processed german features.')

    # English Features
    featuresEN = clean_and_split(read_csvDict(path + '\\features_en.csv', delimiter=';'), ['alt 1', 'alt 2'])
    featuresEN = build_featurelist(featuresEN)
    featuresEN.sort(key=sort_features)
    with open(path + '\\english.features', 'wb') as file:
        pickle.dump(featuresEN, file)
    console_log('Processed english features.')
    return


def build_featurelist(features: list):
    featurelist = []
    for feature in features:
        feats = []
        for alt in feature:
            alts = []
            if alt != 'name':
                for word in feature[alt]:
                    alts.append(process_cue(word))
                feats.append(alts)
        featurelist.append( {'name': feature['name'], 'feature': feats} )
    return featurelist


def clean_and_split(features: list, keylist: list):
    for feature in features:
        for key in keylist:
            if feature[key] == '':
                del feature[key]
            else:
                feature[key] = split_words(feature[key])
    return features


def process_cue(text: str):
    word = '.*'
    tag = '.*'
    lemma = '.*'
    searchTAG = re.findall(r'^([A-Z\*0-9\.\,\(\)]{2,6})\[(.*)\].*$', text)
    searchWORD = re.findall(r'^\{(.*)\}$', text)
    if len(searchTAG) > 0:
        tag = searchTAG[0][0]
        lemma = searchTAG[0][1]
    elif len(searchWORD) > 0:
        word = searchWORD[0]
    else:
        lemma = text
    return {'word': word, 'tag': tag, 'lemma': lemma}


def split_words(text: str):
    if text == '':
        return []
    return text.split(' + ')


def sort_features(element):
    n = element['name'].lower()
    for i in range(len(n)):
        if n[i].isalpha():
            return n[i]
    return n[0]


if __name__ == '__main__':
    main()