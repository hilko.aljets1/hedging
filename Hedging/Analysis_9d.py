from helper import *
import nltk
import statistics

def main():
    results = [{'name': file['filename'][:3], 
                'genre': file['dir'].replace('Results\\', '')[:-3],
                'lang': file['dir'][-2:].upper(),
                'path': file['path']} for file in collect_files('Results', 'csv') if file['dir'].find('Angewandte') == -1]
    
    for result in results:
        csvDict = read_csvDict(result['path'])
        num_sentences = len(csvDict)
        nbc = nltk.FreqDist([s['NBC_Label'] for s in csvDict])
        svc = nltk.FreqDist([s['SVC_Label'] for s in csvDict])
        compare = nltk.FreqDist([s['compare'] for s in csvDict])
        
        result['num sentences'] = num_sentences
        result['NBC_num'] = check_value(nbc.get('HEDGING'))
        result['NBC_per'] = nbc.freq('HEDGING')
        result['SVC_num'] = check_value(svc.get('HEDGING'))
        result['SVC_per'] = svc.freq('HEDGING')
        result['comp_num'] = check_value(compare.get('True'))
        result['comp_per'] = compare.freq('True')

    write_csvDict(results, 'Analysis/Genres/genres.csv')
    
    genre_analysis(results)
    direct_comparsion(results)
    collect_sentences()
    return


def collect_sentences():
    results = [{'name': file['filename'][:3], 
                'genre': file['dir'].replace('Results\\', '')[:-3],
                'lang': file['dir'][-2:].upper(),
                'path': file['path']} for file in collect_files('Results', 'csv') if file['dir'].find('Angewandte') == -1]
    
    genres = ['TB-S1', 'TB-S2', 'ASL-DE', 'ASL-EN', 'PSL-DE', 'PSL-EN']
    collection_NBC = {}
    collection_SVC = {}
    for genre in genres:
        collection_NBC[genre] = []
        collection_SVC[genre] = []

    for result in results:
        report = read_csvDict(result['path'])
        num_sentences = len(report)
        genre = result['genre'] + '-' + result['name'][:2]
        
        for sentence in report:
            if sentence['NBC_Label'] == 'HEDGING':
                collection_NBC[genre].append(int(sentence['num sentence'])/num_sentences)
            if sentence['SVC_Label'] == 'HEDGING':
                collection_SVC[genre].append(int(sentence['num sentence'])/num_sentences)

    nbc = []
    svc = []
    for genre in genres:
        if genre.startswith('T'):
            continue
        for s in collection_NBC[genre]:
            nbc.append({'cat': genre, 'genre': genre[:-3], 'val': s})
        for s in collection_SVC[genre]:
            svc.append({'cat': genre, 'genre': genre[:-3], 'val': s})
    
    write_csvDict(nbc, f'Analysis/Sentences/NBC.csv')
    write_csvDict(svc, f'Analysis/Sentences/SVC.csv')
    return


def direct_comparsion(results):
    german = ['DE'+str(i) for i in range(1, 8)]
    english = ['EN'+str(i) for i in range(1, 7)]
    
    comparsion = {}
    for name in english:
        comparsion[name] = {'name': name, 'lang': 'EN'}
    for name in german:
        comparsion[name] = {'name': name, 'lang': 'DE'}

    for r in results:
        if r['name'].startswith('S'):
            continue
        comparsion[r['name']][r['genre'] + '_NBC_per'] = r['NBC_per']
        comparsion[r['name']][r['genre'] + '_SVC_per'] = r['SVC_per']

    comparsion = list(comparsion.values())
    
    for text in comparsion:
        text['diff_NBC'] = text['ASL_NBC_per'] - text['PSL_NBC_per']
        text['diff_SVC'] = text['ASL_SVC_per'] - text['PSL_SVC_per']

    write_csvDict(comparsion, 'Analysis/Vergleich/comparsion.csv')
    for lang in ['EN', 'DE']:
        write_csvDict([c for c in comparsion if c['lang']== lang], f'Analysis/Vergleich/comparsion-{lang}.csv')

    return


def genre_analysis(results: list[dict]):
    classes = ['NBC', 'SVC', 'comp']
    genres = nltk.FreqDist([s['genre'] for s in results])
    genres_langs = nltk.FreqDist([(s['genre'], s['lang']) for s in results])

    for gl in genres_langs:
        write_csvDict([result for result in results 
                       if result['genre'] == gl[0] and result['lang']== gl[1]], f'Analysis/Genres/{gl[0]}+{gl[1]}.csv')
    
    for g in genres:
        write_csvDict([result for result in results 
                       if result['genre'] == g], f'Analysis/Genres/{g}.csv')

    results_genres = []
    for genre in genres.keys():
        num = {'NBC': [], 'SVC': [], 'comp': []}
        per = {'NBC': [], 'SVC': [], 'comp': []}
        for result in results:
            if result['genre'] != genre:
                continue
            for cl in classes:
                num[cl].append(result[cl + '_num'])
                per[cl].append(result[cl + '_per'])
        
        genre_result = {'genre': genre}
        for cl in classes:
            genre_result[cl + '_num_MW'] = statistics.mean(num[cl])
            
            genre_result[cl + '_num_SD'] = statistics.stdev(num[cl])
            genre_result[cl + '_per_MW'] = statistics.mean(per[cl])
            genre_result[cl + '_per_SD'] = statistics.stdev(per[cl])
        
        nr = {'ASL': 2, 'PSL': 3, 'TB': 1}
        genre_result['nr'] = nr[genre]
        results_genres.append(genre_result)

    
    write_csvDict(results_genres, f'Analysis/Genres/genres_meta.csv')
    return


def check_value(value):
    if value is None:
        return 0
    else:
        return value


if __name__ == '__main__':
    main()
