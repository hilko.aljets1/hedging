from helper import *
import statistics
import numpy as np

def main():
    scores = ['#TP', '#TN', '#FP', '#FN', 'SEN', 'SPE', 'PRÄ', 'GEN', 'F-Score']

    NBC_statistic(scores)
    SVC_statistic(scores)
    return


def SVC_statistic(scores: list[str]):
    files = collect_files('Cross Validation/LinearSVC', 'csv')

    resultsEN = []
    resultsDE = []
    
    for file in files:
        lang = file['filename'][:2]
        content = read_csvDict(file['path'])    
        result = base_statistic(content, scores)

        result['C']= float(content[0]['C'])
        result['logC']= np.log10(float(content[0]['C']))
        result['w0'] = content[0]['w0']
        result['w1'] = content[0]['w1']
        result['sv0'] = content[0]['sv0']
        result['sv1'] = content[0]['sv1']
        result['n_iter'] = content[0]['n_iter']


        if lang == 'en':
            resultsEN.append(result)
        elif lang == 'de':
            resultsDE.append(result)
    
    resultsEN.sort(key=lambda d: d['C'])
    resultsDE.sort(key=lambda d: d['C'])

    write_csvDict(resultsEN, 'Analysis/Cross Validation/LinearSVC-en.csv')
    write_csvDict(resultsDE, 'Analysis/Cross Validation/LinearSVC-de.csv')


def NBC_statistic(scores: list[str]):
    files = collect_files('Cross Validation/NBC', 'csv')

    for file in files:
        lang = file['filename'][:2]
        content = read_csvDict(file['path'])    
        result = base_statistic(content, scores)
        write_csvDict([result], f'Analysis/Cross Validation/NBC-{lang}.csv')



def base_statistic(content: list[dict], scores: list[str]):
    data = {}
    for score in scores:
        data[score] = []

    for line in content:
        for score in scores:
            data[score].append(float(line[score]))

    result = {}
    
    for score in scores:
        result[score + '_MW'] = statistics.fmean(data[score])
        result[score + '_SD'] = statistics.stdev(data[score])
    
    return result



if __name__ == '__main__':
    main()
