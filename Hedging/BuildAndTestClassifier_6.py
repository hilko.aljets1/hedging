from helper import *
import random
import pickle
from nltk.classify import ClassifierI, NaiveBayesClassifier, SklearnClassifier
from sklearn.svm import SVC
import numpy as np


def main():
    ## Splits
    create_dir('Cross Validation')
    k = 5
    splitEN = split_corpus('BioScope/bioscope_en.fset', k)
    with open('Cross Validation/english.split', 'wb') as file:
        pickle.dump(splitEN, file)
    
    splitDE = split_corpus('BioScope/bioscope_de.fset', k)
    with open('Cross Validation/german.split', 'wb') as file:
        pickle.dump(splitDE, file)

    ## NBC
    create_dir('Cross Validation/NBC')
    console_log('Cross Validation/NBC/en')
    write_csvDict(cross_validation('NBC', splitEN), 'Cross Validation/NBC/en.csv')
    console_log('Cross Validation/NBC/de')
    write_csvDict(cross_validation('NBC', splitDE), 'Cross Validation/NBC/de.csv')

    ## LinearSVC
    create_dir('Cross Validation/LinearSVC')  
    start = -3
    end = 3
    num = 2 * (end-start) + 1
    c_range = np.logspace(start, end, num)
    for c in c_range:
        console_log(f'Cross Validation/LinearSVC/en_{np.log10(c)}')
        write_csvDict(cross_validation('LinearSVC', splitEN, c=c), f'Cross Validation/LinearSVC/en_10E{np.log10(c)}.csv')
        console_log(f'Cross Validation/LinearSVC/de_{np.log10(c)}')
        write_csvDict(cross_validation('LinearSVC', splitDE, c=c), f'Cross Validation/LinearSVC/de_10E{np.log10(c)}.csv')
    return


def cross_validation(classifierType: str, split: list[list], c = 1):
    '''Cross validation of ``classifierType`` ('NBC' or 'LinearSVC').'''
    num = len(split)
    results = []

    for i in range(num):
        testing_set = [(sentence['features'], sentence['category']) for sentence in split[i]]
        training_set = []
        for j in range(num):
            if j == 1:
                pass
            else:
                training_set = training_set + [(sentence['features'], sentence['category']) for sentence in split[j]]
        
        if classifierType == 'NBC':
            classifier = NaiveBayesClassifier.train(training_set)
        elif classifierType == 'LinearSVC':
            classifier = SklearnClassifier(SVC(C=c, kernel='linear', class_weight='balanced', cache_size=2000)).train(training_set)
        else:
            raise ValueError(classifierType)
        
        c_matrix = confusion_matrix(classifier, testing_set)

        tp = c_matrix['TP']
        tn = c_matrix['TN']
        fp = c_matrix['FP']
        fn = c_matrix['FN']

        results.append({'Test': i+1, 
                        '#train': len(training_set), 
                        '#hedging train': len([s for s in training_set if s[1] == 'HEDGING']), 
                        '#no-hedging train': len([s for s in training_set if s[1] == 'NO-HEDGING']), 
                        '#test': len(testing_set),  
                        '#hedging test': len([s for s in testing_set if s[1] == 'HEDGING']),
                        '#no-hedging test': len([s for s in testing_set if s[1] == 'NO-HEDGING']),  
                        '#TP': tp,
                        '#TN': tn,
                        '#FP': fp,
                        '#FN': fn,
                        'SEN': tp / (tp + fn),
                        'SPE': tn / (tn + fp),
                        'PRÄ': tp / (tp + fp),
                        'GEN': (tp + tn) / (tp + tn + fp + fn),
                        'F-Score': 2 * tp / (2 * tp + fp + fn),
                        })
        if classifierType == 'LinearSVC':
            results[-1]['C'] = c
            results[-1]['w0'] = classifier._clf.class_weight_[0]
            results[-1]['w1'] = classifier._clf.class_weight_[1]
            results[-1]['sv0'] = classifier._clf.n_support_[0]
            results[-1]['sv1'] = classifier._clf.n_support_[1]
            results[-1]['n_iter'] = classifier._clf.n_iter_[0]

    return results


def confusion_matrix(classifier: ClassifierI, testset: list[dict[str,str]]):
    cmatrix = {'TP': 0, 'TN': 0, 'FP': 0, 'FN': 0}

    for sentence in testset:
        guess = classifier.classify(sentence[0])
        if guess == sentence[1]:
            if sentence[1] == 'HEDGING':
                cmatrix['TP'] += 1
            else:
                cmatrix['TN'] += 1
        else:
            if sentence[1] == 'HEDGING':
                cmatrix['FN'] += 1
            else:
                cmatrix['FP'] += 1
    return cmatrix


def split_corpus(filepath: str, num: int):
    '''Splits corpus randomly into ``num`` sets. Returns list containing ``num`` lists.'''
    corpus = read_pickledFile(filepath)
    random.shuffle(corpus)

    pos = []
    neg = []
    
    for sentence in corpus:
        if sentence['category'] == 'HEDGING':
            pos.append(sentence)
        else:
            neg.append(sentence)

    random.shuffle(pos)
    random.shuffle(neg)

    splits = []
    for i in range(num):
        splits.append([])
    
    for i in range(len(pos)):
        splits[i % num].append(pos[i])
    for i in range(len(neg)):
        splits[i % num].append(neg[i])

    random.shuffle(splits)
    for i in range(num):
        random.shuffle(splits[i])

    return splits


if __name__ == '__main__':
    main()
