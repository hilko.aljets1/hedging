from helper import *
import re
import tqdm
import pickle


def main():
    #Featurelists
    featuresEN = read_pickledFile('Features/english.features')
    featuresDE = read_pickledFile('Features/german.features')

    # BioScope
    dir = 'BioScope'
    build_featureset(os.path.join(dir, 'bioscope_en.tagged'), featuresEN)
    build_featureset(os.path.join(dir, 'bioscope_de.tagged'), featuresDE)

    # Testcorpus
    testcorpus = collect_files('Corpus/Sentences', ending='tagged')
    for file in testcorpus:
        language = file['dir'][-2] + file['dir'][-1]
        if language == 'en':
            build_featureset(file['path'], featuresEN)
        elif language == 'de':
            build_featureset(file['path'], featuresDE)
    return


def build_featureset(filepath: str, featurelist: list):
    '''Returns a list. Every item is the dict of a sentence including 
    at least ``sentence`` (tagged) and ``features`` (the featurelist of the sentence)'''
    document = read_pickledFile(filepath)
    filepath_fset = os.path.splitext(filepath)[0] + '.fset'
    fset = []

    for sentence in tqdm.tqdm(document,
                            desc=f'{current_time()} | Building featureset for {filepath}',
                            dynamic_ncols=True,
                            unit='sentence'):
        if type(sentence) == dict:
            fset.append(sentence)
            fset[-1]['features'] = find_features(sentence['sentence'], featurelist)
        elif type(sentence) == list:
            fset.append( {'sentence': sentence, 'features': find_features(sentence, featurelist)} )

    with open(filepath_fset, 'wb') as file:
        pickle.dump(fset, file)

    return fset


def find_features(sentence: list[dict[str, str]], featurelist: list[dict[str, str]]):
    '''Searches for every feature (and its alternatives, if existing) of the featurelist in the given ``sentence``.'''
    features = {}
    for feature in featurelist:
        name = feature['name']
        for alternative in feature['feature']:
            found = True
            for word in alternative:
                found = found & find_tagged_word(sentence, word)
            if found:
                break
        features[name] = found

    return features


def find_tagged_word(sentence: list[dict[str, str]], word: dict[str, str]):
    '''Searches for a word tuple in a sentence.'''
    for _word in sentence:
        found = 0
        for key in word:
            if word[key] == '.*':
                found += 1
                continue
            if word[key] == _word[key]:
                found += 1
                continue
            if '.' in word[key] or '*' in word[key]:
                if re.findall('^{}$'.format(word[key]), _word[key]):
                    found += 1
        if found == 3:
            return True
    return False


if __name__ == "__main__":
    main()
