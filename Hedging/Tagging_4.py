from helper import *
import os
import pickle
from tqdm import tqdm
from nltk import word_tokenize
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import treetaggerwrapper as tt

def main():
    taggerEN = get_treetagger('en')
    taggerDE = get_treetagger('de')

    # BioScope
    dir = 'BioScope'
    tagging_document(os.path.join(dir, 'bioscope_en.csv'), taggerEN, 'sentence')
    tagging_document(os.path.join(dir, 'bioscope_de.csv'), taggerDE, 'sentence')

    # Testcorpus
    testcorpus = collect_files('Corpus/Sentences', ending='txt')
    for file in testcorpus:
        language = file['dir'][-2] + file['dir'][-1]
        if language == 'en':
            tagging_document(file['path'], taggerEN)
        elif language == 'de':
            tagging_document(file['path'], taggerDE)
    return


def tagging_document(filepath: str, tagger: tt.TreeTagger, key=''):
    document = get_document(filepath)
    filepathTagged = os.path.splitext(filepath)[0] + '.tagged'
    tagged = []

    with open(filepathTagged, 'wb') as file:
        for sentence in tqdm(document,
                                  desc=f'{current_time()} | Tagging {filepath}, lang = {tagger.lang}',
                                  dynamic_ncols=True,
                                  total=len(document),
                                  unit='sentence'):
            if type(sentence) == dict:
                tagged.append(sentence)
                tagged[-1][key] = tagging_sentence(sentence[key], tagger)
            elif type(sentence) == str:
                tagged.append(tagging_sentence(sentence, tagger))
            else:
                raise ValueError
            pickle.dump(tagged[-1], file)
    return tagged


def tagging_sentence(sentence: str, tagger: tt.TreeTagger):
    '''Using NLTK word_tokenize to split ``sentence`` into words and TreeTagger ``tagger`` to tag it.'''
    language = tagger.lang
    if language == 'en':
        _lang = 'english'
    elif language == 'de':
        _lang = 'german'

    tagged = tagger.tag_text(word_tokenize(sentence, _lang), tagonly=True)
    return [{'word': word.word, 'tag': word.pos, 'lemma': word.lemma} for word in tt.make_tags(tagged)]


def get_treetagger(language: str):
    '''Init of the TreeTagger for the given language.
    TreeTagger must be installed at 'C:/TeeTagger'.
    Uses parfile 'english-bnc.par' or 'german.par'. '''
    if language == 'en':
        parfile = 'english-bnc.par'
    elif language == 'de':
        parfile = 'german.par'
    else:
        raise ValueError('Wrong language')
    return tt.TreeTagger(TAGLANG=language, TAGPARFILE=parfile)


if __name__ == "__main__":
    main()
