from helper import *
import nltk

def main():
    langs = ['EN', 'DE']
    classifierTypes = ['NBC', 'SVC']

    classifiers = {}

    for lang in langs:
        for classifierType in classifierTypes:
            classifiers[f'{lang},{classifierType}'] = read_pickledFile(f'Classifier/{classifierType.lower()}-{lang.lower()}.classifier')

    bioscopeEN = read_pickledFile('BioScope/bioscope_en.fset')
    bioscopeDE = read_pickledFile('BioScope/bioscope_de.fset')
    bioscope = {'EN': bioscopeEN, 'DE': bioscopeDE}

    bioscope_all(bioscope, classifiers, classifierTypes, langs)
    bioscope_lang(bioscope, classifiers, classifierTypes, langs)
    bioscope_classifier(bioscope, classifiers, classifierTypes, langs)
    return


def bioscope_all(bioscope, classifiers, classifierTypes, langs):
    results = []

    for i in range(len(bioscope['EN'])):
        result_sentence = {'sentence': i+1, 'cat': bioscope['EN'][i]['category']}
        result = {}
        for combi in classifiers.keys():
            c = combi.replace(',', '-')
            result[c] = classifiers[combi].classify(bioscope[combi[:2]][i]['features'])
            result_sentence[c] = result[c]


        if len(set(result.values())) == 1:
            if list(result.values())[0] == 'HEDGING':
                result_sentence['compare'] = '[++]'
            else:
                result_sentence['compare'] = '[--]'
        else:
            if list(result.values()).count('HEDGING') == 1:
                result_sentence['compare'] = '[+] '+ [key for key, value in result.items() if value == 'HEDGING'][0]
            elif list(result.values()).count('HEDGING') == 2:
                pos = [key for key, value in result.items() if value == 'HEDGING']
                pos.sort()
                result_sentence['compare'] = '[+] '+ ' + '.join(pos)
            elif list(result.values()).count('HEDGING') == 3:
                result_sentence['compare'] = '[-] '+ [key for key, value in result.items() if value == 'NO-HEDGING'][0]
        
        results.append(result_sentence)
    write_csvDict(results, 'Analysis/bioscope-all.csv')

    freq = nltk.FreqDist([s['compare'] for s in results])
    print('Results BioScope all', freq.B(), freq.N())
    print(freq.items())

    freq = nltk.FreqDist([(s['cat'], s['compare']) for s in results])
    print('Results BioScope all', freq.B(), freq.N())
    print(freq.items())
    return results


def bioscope_classifier(bioscope, classifiers, classifierTypes, langs):
    results = {}

    for lang in langs:
        results[lang] = []

        for i in range(len(bioscope['EN'])):
            result = {'sentence': i+1, 'cat': bioscope[lang][i]['category']}
            for classifierType in classifierTypes:
                result[classifierType] = classifiers[lang + ',' + classifierType].classify(bioscope[lang][i]['features'])
            
            if result['NBC'] == result['SVC']:
                if result['NBC'] == 'HEDGING':
                    result['compare'] = '+'
                else:
                    result['compare'] = '--'
            else:
                if result['NBC'] == 'HEDGING':
                    result['compare'] = 'NBC'
                else:
                    result['compare'] = 'SVC'
            results[lang].append(result)
        write_csvDict(results[lang], f'Analysis/bioscope-{lang}.csv')
        freq = nltk.FreqDist((s['cat'], s['compare']) for s in results[lang])
        print(lang, freq.N(), freq.B())
        print(freq.items())
        print('')
    
    return results


def bioscope_lang(bioscope, classifiers, classifierTypes, langs):
    results = {}

    for classifierType in classifierTypes:
        results[classifierType] = []

        for i in range(len(bioscope['EN'])):
            result = {'sentence': i+1, 'cat': bioscope['EN'][i]['category']}
            for lang in langs:
                result[lang] = classifiers[lang + ',' + classifierType].classify(bioscope[lang][i]['features'])
            
            if result['EN'] == result['DE']:
                if result['EN'] == 'HEDGING':
                    result['compare'] = '+'
                else:
                    result['compare'] = '--'
            else:
                if result['EN'] == 'HEDGING':
                    result['compare'] = 'EN'
                else:
                    result['compare'] = 'DE'
            results[classifierType].append(result)
        write_csvDict(results[classifierType], f'Analysis/bioscope-{classifierType}.csv')
        freq = nltk.FreqDist((s['cat'], s['compare']) for s in results[classifierType])
        print(classifierType, freq.N(), freq.B())
        print(freq.items())
        print('')
    
    return results


if __name__ == '__main__':
    main()
