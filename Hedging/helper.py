import csv
import time
import pickle
import os
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

def create_dir(dir: str):
    '''Checks if ``dir`` exists, and if not, creates it.'''
    if not dir or dir == '':
        return
    if os.path.isdir(dir):
        return
    os.makedirs(dir)
    return


def collect_files(dir: str, ending: str):
    '''Walks through ``dir`` und collects every file with the given ending.

    If the ``ending`` is empty, it collects every file. Ending can or cannot include a dot (.).
    Returns a list of dictionaries with the name of the directory ``dir``, the ``filename`` and the complete ``path``.
    
    Example output: file 'dir/folder/file.txt' -> dir: 'dir/folder', filename: 'file.txt' and path: 'dir/folder/filename.txt' '''
    files = []
    for dirpath, dirnames, filenames in os.walk(dir):
        for filename in filenames:
            if filename.endswith(ending):
                files.append( {'dir': dirpath, 'filename': filename, 'path': os.path.join(dirpath, filename)} )
    return files


def write_csvDict(dicts: list, filepath: str, encoding='utf-8', header=None):
    '''Writes a csv file with the help of csv.DictWriter.
     
    ``filepath`` should end with '.csv'. 
    ``dicts`` should be a list containg dictionarys with the same keys. 
    Automatically checks if the filepath exists and creates a folder if needed.
    Uses the keys of the first dictonary as csv header, if no list ``header`` is given.'''    
    if len(dicts) < 1:
        return
    create_dir(os.path.split(filepath)[0])
    with open(filepath, 'w', newline='', encoding=encoding) as file:
        if header == None:
            header = dicts[0].keys()
        writer = csv.DictWriter(file, fieldnames=header)
        writer.writeheader()
        for dicts in dicts:
            writer.writerow(dicts)
    return


def write_txt(lst: str|list[str], filepath: str, encoding='utf-8'):
    '''Writes a .txt file. 
    
    If input is a list, every item is seperated by a new line '\\n'.
    Automatically checks if the filepath exists and creates a folder if needed.'''
    create_dir(os.path.split(filepath)[0])
    with open(filepath, 'w', encoding=encoding) as file:
        for item in lst:
            if type(item) == list and type(item[0]) == str:
                file.write(' '.join(item))
            elif type(item) == str:
                file.write(item)
            else:
                file.write(str(item))
            file.write('\n')


def read_csvDict(filepath: str, encoding='utf-8'):
    '''Reads a .csv file and returns it as a list with dictionaries.'''
    csvlist = []
    with open(filepath, encoding=encoding) as file:
        reader = csv.DictReader(file)
        for row in reader:
            csvlist.append(row)
    return csvlist


def read_fileByLine(filepath: str, encoding='utf-8'):
    '''Reads a file line by line and returns a list with every line as item.'''
    list = []
    with open(filepath, encoding=encoding) as file:
        for line in file:
            list.append(line.strip())
    return list


def read_pickledFile(filepath: str):
    '''Reads a pickled file line by line and returns a list with every line as item.'''
    content = []
    with open(filepath, 'rb') as file:
        try:
            while True:
                content.append(pickle.load(file))
        except EOFError:
            pass
    if len(content) == 1:
        return content[0]
    return content  
    

def console_log(*text: str):
    '''Prints log text with current time.'''
    print(current_time(), end=' | ')
    for t in text:
        print(t, end=' ')
    print('')


def current_time():
    '''Returns current time as str.'''
    return time.strftime("%H:%M:%S", time.localtime())


def get_document(filepath: str):
    ''' Checks if file at ``filepath`` is a .csv or .txt file, reads the file accordingly, and returns it.'''
    fileext = os.path.splitext(filepath)[1]
    file = []
    if fileext == '.csv':
        file = read_csvDict(filepath)
    elif fileext == '.txt':
        file = read_fileByLine(filepath)
    else:
        raise ValueError(f'Filetype {fileext} not supported. Only .txt and .csv possible.')
    return file


def words_to_sentence(list_of_words: list):
    '''Joins a list of words to a sentence. Checks for whitespace at common punctuation (``.,()[]-?!``).'''
    sentence = ' '.join(list_of_words)
    sentence = sentence.replace(' .', '.').replace(' ,', ',').replace('( ', '(').replace(' )', ')').replace('[ ', '[').replace(' ]', ']').replace(' -', '-').replace(' ?', '?').replace(' !', '!')
    return sentence
