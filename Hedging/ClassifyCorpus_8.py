from helper import *
from nltk.classify import ClassifierI
from tqdm import tqdm


def main():
    corpus = collect_files('Corpus', 'fset')
    classifiersEN = {}
    classifiersDE = {}
    
    for classifier in collect_files('Classifier', 'classifier'):
        typ = classifier['filename'][:3].upper()
        lang = classifier['filename'][4:6].lower()
        if lang == 'en':
            classifiersEN[typ] = read_pickledFile(classifier['path'])
        elif lang == 'de':
            classifiersDE[typ] = read_pickledFile(classifier['path'])

    for file in tqdm(corpus,
                     desc=f'{current_time()} | Analyzing corpus',
                     dynamic_ncols=True,
                     total=len(corpus),
                     unit='document'):
        _lang = file['dir'][-2:].lower()
        content = read_pickledFile(file['path'])
        outputPath = file['path'].replace('Corpus\\Sentences\\','').replace('.fset','.csv')
        outputPath = f'Results/{outputPath}'

        if _lang == 'en':
            result = classify_document(content, classifiersEN['NBC'], classifiersEN['SVC'])
        elif _lang == 'de':
            result = classify_document(content, classifiersDE['NBC'], classifiersDE['SVC'])
        write_csvDict(result, outputPath)
    return


def classify_document(document: list, nbc: ClassifierI, svc: ClassifierI):
    results = []
    i = 0
    for sentence in document:
        i += 1
        label_nbc = nbc.classify(sentence['features'])
        label_svc = svc.classify(sentence['features'])
        results.append( {'num sentence': i,
                        'sentence': words_to_sentence([word['word'] for word in sentence['sentence']]),
                        'true features': [key for (key, value) in sentence['features'].items() if value],
                        'NBC_Label': label_nbc,
                        'NBC_prob': nbc.prob_classify(sentence['features']).prob('HEDGING'),
                        'SVC_Label': label_svc,
                        'compare': label_nbc == label_svc
                        } )
    return results


if __name__ == '__main__':
    main()
