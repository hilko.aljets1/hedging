from helper import *
from os.path import join
from xml.etree import ElementTree as et


def main():
    dir = 'BioScope'

    ## English Corpus
    fileArticlesEN = 'bioscope_articles_en.xml'
    fileAbstractsEN = 'bioscope_abstracts_en.xml'
    sentencesEN, hedgingEN = process_english_corpus(et.parse(join(dir, fileArticlesEN)), et.parse(join(dir, fileAbstractsEN)))
    write_csvDict(sentencesEN, join(dir, 'bioscope_en.csv'))
    console_log('Processing english bioscope corpus done.')

    ## German Corpus
    fileBioscopeDE = 'bioscope_de.txt'
    sentencesDE = process_german_corpus(join(dir, fileBioscopeDE), join(dir, 'bioscope_en.csv'))
    write_csvDict(sentencesDE, join(dir, 'bioscope_de.csv'))
    console_log('Processing german bioscope corpus done.')
    return


def process_german_corpus(german_txt: str, english_csv: str):
    de_txt = read_fileByLine(german_txt)
    en_csv = read_csvDict(english_csv)
    de_csv = []

    for i in range(len(en_csv)):
        de_csv.append( {'sentence': de_txt[i], 'category': en_csv[i]['category']} )

    return de_csv


def process_english_corpus(*xml_docs: tuple[et.ElementTree]):
    if len(xml_docs) < 1:
        raise ValueError
    
    sentences, cues  = [], []
    for xml_doc in xml_docs:
        root = xml_doc.getroot()
        for xml_doc in root.iter('Document'):
            for xml_sentence in xml_doc.iter('sentence'):
                sentence = et.tostring(xml_sentence, method='text', encoding='unicode').strip()
                sentences.append({'sentence': sentence, 'category': 'NO-HEDGING'})
                for cue in xml_sentence.iter('cue'):
                    if cue.attrib['type'] == 'speculation':
                        if cue.text.lower() not in cues:
                            cues.append({'cue': cue.text.lower(), 'sentence': sentence})
                        sentences[-1]['category'] = 'HEDGING'
    return (sentences, cues)


if __name__ == '__main__':
    main()
    

