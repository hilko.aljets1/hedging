# Hedging
This project analyzes the use of hedges in scientific texts and is part of the following dissertation (German):
> Aljets, H. (2023). _Adaptierte wissenschaftliche Literatur. Charakteristika und Potential zur Vermittlung von Nature of Science_. Georg-August-Universität Göttingen, Göttingen. https://dx.doi.org/10.53846/goediss-10297


## Requirements
Several packages are used:
* [trafilatura](https://trafilatura.readthedocs.io/)
* [NLTK](https://www.nltk.org/)
* [treetaggerwrapper](https://treetaggerwrapper.readthedocs.io/)
* [tqdm](https://tqdm.github.io/)
* [scikit-learn](https://scikit-learn.org/)
* [numpy](https://numpy.org/)

Additionally, the software [TreeTagger](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger) (Schmid, 1994, _Proceedings of International Conference on New Methods in Language Processing_) is needed. For English, the BNC parameter file, and for German, the standard parameter file, is used.

For training, the BioScope database was used (without clinical articles) (Szarvas et al., [2008](https://doi.org/10.3115/1572306.1572314), _Proceedings of the Workshop on Current Trends in Biomedical Natural Language Processing_ (pp. 38–45)).


## Workflow
### Data Preprocessing
<p align="center" width = 100%>
    <img src="Hedging-Workflow-1.svg" alt="Workflow of the data preprocessing" width=66%/>
</p>

### Classification
<p align="center" width = 100%>
    <img src="Hedging-Workflow-2.svg" alt="Workflow of the classification" width=66%/>
</p>


## Directory structure
* [BioScope](Hedging/BioScope)
  * Insert the BioScope XML Data and the translation
* [Corpus](Hedging/Corpus)
  * [HTM](Hedging/Corpus/HTM)
    * Insert your articles in subfolders (`/category_langid/article-name.htm`)
  * [Raw](Hedging/Corpus/Raw)
    * Insert your articles in subfolders (`/category_langid/article-name.txt`)
* [Features](Hedging/Features)
  * [english.features](Hedging/Features/english.features)
  * [features_de.csv](Hedging/Features/features_de.csv)
  * [features_en.csv](Hedging/Features/features_en.csv)
  * [german.features](Hedging/Features/german.features)
* Python Scripts
